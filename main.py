from tww.tokenizer import parse_args, setup_logging_level, main

if __name__ == "__main__":
    args = parse_args()
    setup_logging_level(args.debug)
    main(args)
