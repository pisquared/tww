from tww import dateparser_parse_dt, get_utcnow, dt_tz_translation, get_local_tzname_iana, get_local_tz_offset, \
    tzname_to_tz_offset, get_s_since_epoch, time_to_emoji

if __name__ == "__main__":
    print(get_local_tzname_iana())

    print(get_local_tz_offset())

    print(tzname_to_tz_offset(
        get_local_tzname_iana()
    ))

    print(dt_tz_translation(
        get_utcnow(),
        get_local_tz_offset()
    ))

    print(time_to_emoji(
        dt_tz_translation(
            get_utcnow(),
            get_local_tz_offset()
        )
    ))

    print(get_s_since_epoch(
        dateparser_parse_dt("2019-12-11 15:53:40+0000")
    ))

    # QUERIES....

    "<TIME>"
    "================="
    "2019-12-12T03:14:15"  # ISO-8601 Naive time
    "2019-12-12T03:14:15+0000"  # ISO-8601 Timezone aware

    "<TIME> IN <WHEREVER>"
    "================="
    "Now in epoch"  # Absolute:Epoch
    "Now in New York"  # Location:City
    "Now in Bulgaria"  # Location:Country
    "Now in USA"  # Location:Country       - multiple timezones
    "Now in CET"  # Timezone:Abbreviation  -
    # https://en.wikipedia.org/wiki/List_of_time_zone_abbreviations
    "Now in Europe/Zurich"  # Timezone:IANA          - tz db,
    # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones, timezone
    # database
    "Now in Alfa"  # Timezone:Military      -
    # https://en.wikipedia.org/wiki/List_of_military_time_zones
    "Now in +02:00"  # Timezone:Offset        -
    # https://en.wikipedia.org/wiki/List_of_UTC_time_offsets

    "<TIME> IN <WHEREVER> TO <WHEREVER>"
    "=================================="
    "Now in Bulgaria to UTC"  # == "Now in UTC"
    "2019-12-12 03:14:15+03:00 to New York"  # Something in +03:00 to
    # Location:City

    "<TIMEDELTA>"
    "================="
    "[Time] since <TIME>"  # now-<TIME>                                  ->
    # timedelta
    "[Time] until <TIME>"  # <TIME>-now                                  ->
    # timedelta
    "[Time] between 27-01-1992 and 09 May 1997"  # == end_dt - start_dt  ->
    # timedelta

    "<TIME CALCULATION>"
    "================="
    "in 3 hours"  # == now+3 hours                    -> datetime
    "5 days ago"  # == now-5 days                     -> datetime
    "12-12-2019 + 2 weeks"  # == dt + timedelta       -> datetime
    "05:23 - 150 minutes"  # == dt - timedelta        -> datetime

    # strptime: str -> datetime
    # strftime: datetime -> str

    #     %Y  Year with century as a decimal number.
    #     %m  Month as a decimal number [01,12].
    #     %d  Day of the month as a decimal number [01,31].
    #     %H  Hour (24-hour clock) as a decimal number [00,23].
    #     %M  Minute as a decimal number [00,59].
    #     %S  Second as a decimal number [00,61].
    #     %z  Time zone offset from UTC.
    #     %a  Locale's abbreviated weekday name.
    #     %A  Locale's full weekday name.
    #     %b  Locale's abbreviated month name.
    #     %B  Locale's full month name.
    #     %c  Locale's appropriate date and time representation.
    #     %I  Hour (12-hour clock) as a decimal number [01,12].
    #     %p  Locale's equivalent of either AM or PM.

    # https://docs.python.org/3/library/datetime.html
    #
    # %a		Weekday as locale’s abbreviated name.
    # "Sun, Mon, …, Sat (en_US); So, Mo, …, Sa (de_DE)
    # %A		Weekday as locale’s full name.
    # Sunday, Monday, …, Saturday (en_US);	Sonntag, Montag, …, Samstag (de_DE)
    # %w		Weekday as a decimal number, where 0 is Sunday and 6 is
    # Saturday.		0, 1, …, 6
    # %d		Day of the month as a zero-padded decimal number.
    # 01, 02, …, 31
    # %b		Month as locale’s abbreviated name.
    # Jan, Feb, …, Dec (en_US);	Jan, Feb, …, Dez (de_DE)
    # %B		Month as locale’s full name.
    # January, February, …, December (en_US);	Januar, Februar, …, Dezember (
    # de_DE)
    # %m		Month as a zero-padded decimal number.
    # 01, 02, …, 12
    # %y		Year without century as a zero-padded decimal number.
    # 00, 01, …, 99
    # %Y		Year with century as a decimal number.
    # 0001, 0002, …, 2013, 2014, …, 9998, 9999
    # %H		Hour (24-hour clock) as a zero-padded decimal number.
    # 00, 01, …, 23
    # %I		Hour (12-hour clock) as a zero-padded decimal number.
    # 01, 02, …, 12
    # %p		Locale’s equivalent of either AM or PM.
    # AM, PM (en_US);	am, pm (de_DE)
    # %M		Minute as a zero-padded decimal number.
    # 00, 01, …, 59
    # %S		Second as a zero-padded decimal number.
    # 00, 01, …, 59
    # %f		Microsecond as a decimal number, zero-padded on the left.
    # 000000, 000001, …, 999999
    # %z		UTC offset in the form ±HHMM[SS[.ffffff]]
    #           (empty string if the object is naive).
    #           (empty), +0000, -0400, +1030, +063415, -030712.345216
    # %Z		Time zone name (empty string if the object is naive).
    # (empty), UTC, EST, CST
    # %j		Day of the year as a zero-padded decimal number.
    # 001, 002, …, 366
    # %U		Week number of the year (Sunday as the first day of the week)
    #           as a zero padded decimal number. All days in a new year
    #           preceding the first Sunday are considered to be in week
    #           0.		00, 01, …, 53
    # %W		Week number of the year (Monday as the first day of the week)
    #           as a decimal number. All days in a new year preceding the
    #           first Monday are considered to be in week 0.
    #           00, 01, …, 53
    # %c		Locale’s appropriate date and time representation.
    # Tue Aug 16 21:30:00 1988 (en_US);	Di 16 Aug 21:30:00 1988 (de_DE)
    # %x		Locale’s appropriate date representation.	                           	08/16/1988 (en_US);	16.08.1988 (de_DE)
    # %X		Locale’s appropriate time representation.	                        	21:30:00 (en_US);	21:30:00 (de_DE)
    # %%		A literal '%' character.	                                        	%
