import contextlib
import csv
import locale
import os

from fuzzywuzzy import fuzz

from tww.lib import basepath


@contextlib.contextmanager
def setlocale(*args, **kw):
    saved = locale.setlocale(locale.LC_ALL)
    yield locale.setlocale(*args, **kw)
    locale.setlocale(locale.LC_ALL, saved)


def find_country_alias(locale_s):
    with open(os.path.join(basepath, "data", "country_codes.csv")) as f:
        cfile = csv.reader(f)
        for row in cfile:
            country, alpha2, alpha3 = row[0:3]
            country, alpha2, alpha3 = country.lower(), alpha2.lower(), alpha3.lower()
            if locale_s in [country, alpha2, alpha3]:
                return country, alpha2, alpha3
            fuzz_ratio = fuzz.ratio(locale_s, country)
            if fuzz_ratio > 90:
                return country, alpha2, alpha3
    return None, None, None


def find_language_alias(locale_s):
    with open(os.path.join(basepath, "data", "language_codes.csv")) as f:
        cfile = csv.reader(f)
        for row in cfile:
            name, native_name, a2, a3 = row[1:5]
            name, native_name, a2, a3 = name.lower(), native_name.lower(), a2.lower(), a3.lower()
            if locale_s in [a2, a3, name, native_name]:
                return name, native_name, a2, a3
            fuzz_ratio = fuzz.ratio(locale_s, name)
            if fuzz_ratio > 90:
                return name, native_name, a2, a3
            fuzz_ratio = fuzz.ratio(locale_s, native_name)
            if fuzz_ratio > 80:
                return name, native_name, a2, a3
    return None, None, None, None


def lc_time_to_codes(lc_time):
    country_lang, encoding = lc_time.split('.')
    country_code, lang_code = country_lang.split('_')
    return country_code, lang_code, encoding


def get_default_locale():
    default_locale = locale.getlocale()
    if type(default_locale) == tuple:
        default_locale = "{}.{}".format(*default_locale)
    country_code, lang_code, encoding = lc_time_to_codes(default_locale)
    return country_code, lang_code, encoding, default_locale


def resolve_locale(locale_s=None):
    country_code, lang_code, encoding, default_locale = get_default_locale()
    rv = dict(
        query=locale_s,
        country_code=country_code,
        lang_code=lang_code,
        encoding=encoding,
        lc_time=default_locale,
    )
    default_encoding = 'utf-8'
    if not locale_s:
        return rv
    if '.' in locale_s:
        country_lang, encoding = locale_s.split('.')
    else:
        country_lang, encoding = locale_s, default_encoding
    if '_' in country_lang:
        country_code, lang_code = country_lang.split('_')
        if len(country_code) == 2 and len(lang_code) == 2:
            try:
                lc_time = "{}_{}.{}".format(country_code, lang_code, encoding)
                locale.setlocale(locale.LC_TIME, lc_time)
                rv["country_code"] = country_code
                rv["lang_code"] = lang_code
                rv["encoding"] = encoding
                rv["lc_time"] = lc_time
                return rv
            except:
                ...
    locale_s = locale_s.strip().lower()
    country, alpha2, alpha3 = find_country_alias(locale_s)
    lang_name, lang_native_name, lang2, lang3 = find_language_alias(locale_s)
    if alpha2:
        locale_hypotheses = {k: v for k, v in locale.locale_alias.items() if k.startswith(alpha2)}
        for k, v in locale_hypotheses.items():
            lower = k.lower()
            if 'utf-8' in lower:
                rv["lc_time"] = v
                break
        else:
            if locale_hypotheses:
                lc_time = locale_hypotheses.get(alpha2)
                if lc_time:
                    country_code, lang_code, encoding = lc_time_to_codes(lc_time)
                    rv["country_code"] = country_code
                    rv["lang_code"] = lang_code
                    rv["encoding"] = encoding
                    rv["lc_time"] = lc_time
                    return rv
    if lang2:
        locale_hypotheses = {k: v for k, v in locale.locale_alias.items() if k.startswith(lang2)}
        for k, v in locale_hypotheses.items():
            lower = k.lower()
            if 'utf-8' in lower:
                rv["lc_time"] = v
                break
        else:
            if locale_hypotheses:
                lc_time = locale_hypotheses.get(lang2)
                if lc_time:
                    country_code, lang_code, encoding = lc_time_to_codes(lc_time)
                    rv["country_code"] = country_code
                    rv["lang_code"] = lang_code
                    rv["encoding"] = encoding
                    rv["lc_time"] = lc_time
                    return rv
    return rv