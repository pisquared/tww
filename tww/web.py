import pytz
from flask import Flask, render_template, jsonify, request

from tokenizer import resolve_query, QUERY_TYPE_DT, QUERY_TYPE_DT_TR, QUERY_TYPE_TZ, QUERY_TYPE_TD

app = Flask(__name__)

import dateparser
from tww.lib import resolve_timezone

IN_KW = " in "
TO_KW = " to "
NO_TZ_FORMAT = '%Y-%m-%dT%H:%M:%S'
ISO_FORMAT = '%Y-%m-%dT%H:%M:%S%z'
DATE_FORMAT = '%Y-%m-%d'
TIME_FORMAT = '%H:%M:%S'
TZ_OFFSET_FORMAT = '%z'


def parse_query(q):
    fmt = NO_TZ_FORMAT
    to_split = q.split(TO_KW)
    in_dt_resolve_fmt, to_dt_resolve_fmt = {}, {}
    in_tz_resolve, to_tz_resolve = {}, {}
    error = ""
    if len(to_split) >= 2:
        # 18:00 to Zurich
        in_q, to_tz = to_split[0], TO_KW.join(to_split[1:])
    else:
        # 18:00
        in_q = q
        to_tz = ""
    in_split = in_q.split(IN_KW)
    if len(in_split) >= 2:
        # 18:00 in Zurich
        in_dt, in_tz = in_split[0], IN_KW.join(in_split[1:])
    else:
        # in 2 hours / 19:00 CET
        in_dt = in_q
        in_tz = ""
    try:
        in_dt_resolve = dateparser.parse(in_dt, settings={
            'RETURN_AS_TIMEZONE_AWARE': True})
        if not in_tz:
            in_tz = in_dt_resolve.strftime("%Z")
        if in_tz:
            isofmt = in_dt_resolve.isoformat()
            in_tz_resolve = resolve_timezone(in_tz)
            in_dt_resolve = dateparser.parse(isofmt, settings={
                'TO_TIMEZONE': in_tz_resolve})
        to_dt_resolve, to_tz_resolve = in_dt_resolve, in_tz_resolve
        if in_dt_resolve:
            in_dt_resolve_fmt = {
                'iso': in_dt_resolve.strftime(ISO_FORMAT),
                'fmt': in_dt_resolve.strftime(fmt),
                'date': in_dt_resolve.strftime(DATE_FORMAT),
                'time': in_dt_resolve.strftime(TIME_FORMAT),
                'tz_offset': in_dt_resolve.strftime(TZ_OFFSET_FORMAT),
            }
        if to_tz:
            isofmt = in_dt_resolve.isoformat()
            to_tz_resolve = resolve_timezone(to_tz)
            to_dt_resolve = dateparser.parse(isofmt, settings={
                'TO_TIMEZONE': to_tz_resolve})
        if to_dt_resolve:
            to_dt_resolve_fmt = {
                'iso': to_dt_resolve.strftime(ISO_FORMAT),
                'fmt': to_dt_resolve.strftime(fmt),
                'date': to_dt_resolve.strftime(DATE_FORMAT),
                'time': to_dt_resolve.strftime(TIME_FORMAT),
                'tz_offset': to_dt_resolve.strftime(TZ_OFFSET_FORMAT),
            }
    except Exception as e:
        error = str(e)
    return {
        'error': error,
        'query': q,
        'fmt': fmt,
        'in_dt': in_dt,
        'in_tz_resolve': in_tz_resolve,
        'in_dt_resolve': in_dt_resolve_fmt,
        'to_dt_resolve': to_dt_resolve_fmt,
        'in_tz': in_tz,
        'to_tz': to_tz,
        'to_tz_resolve': to_tz_resolve,
    }


@app.route("/")
def home():
    ctx = dict(all_tz=pytz.all_timezones)
    q = request.args.get('q')
    if q:
        ctx["q_resp"] = jsonify(resolve_query(q))
    return render_template("home.html", all_tz=pytz.all_timezones)


def render_solution(solution):
    query_type = solution["query_type"]
    if query_type == QUERY_TYPE_DT:
        return render_template("dt.html", **solution)
    elif query_type == QUERY_TYPE_DT_TR:
        return render_template("dt_tr.html", **solution)
    elif query_type == QUERY_TYPE_TZ:
        return render_template("tz.html", **solution)
    elif query_type == QUERY_TYPE_TD:
        return render_template("td.html", **solution)



def render_solutions(results):
    rv = []
    for solution in results['solutions']:
        rv.append(render_solution(solution))
    return rv


@app.route("/api/datetime")
def api_datetime():
    q = request.args.get('q')
    results = resolve_query(q)
    rendered_solutions = render_solutions(results)
    results['rendered_solutions'] = rendered_solutions
    return jsonify(results)


if __name__ == "__main__":
    app.run(debug=True)
