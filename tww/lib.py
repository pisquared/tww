#!/usr/bin/env python
"""
Find time now, in the past or future in any timezone or location.
"""

import csv
import datetime
import os
import pathlib
import random
import re
import string
from collections import defaultdict
from datetime import datetime, timedelta
from heapq import heappush, heappop

import dateparser
import pytz
from dateparser import parse as parse_dt
from dateparser.search import search_dates
from dateparser.timezone_parser import StaticTzInfo
from datetimerange import DateTimeRange
from dateutil.parser import parse as dutil_parse
from dateutil.tz import gettz, tzlocal
from fuzzywuzzy import fuzz
from geopy import Nominatim
from geopy.exc import GeocoderTimedOut
from pytz import timezone
from pytz.exceptions import UnknownTimeZoneError
from timezonefinder import TimezoneFinder
from word2number import w2n

from tww.common import logger

FUZZ_THRESHOLD = 70
ISO_Z_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
ISO_FORMAT = '%Y-%m-%dT%H:%M:%S%z'
ISO_FORMAT_NO_TZ = '%Y-%m-%dT%H:%M:%S'
DEFAULT_FORMAT = '%Y-%m-%d %H:%M:%S%z'

basepath = os.path.dirname(os.path.abspath(__file__))

SKIP_DATE_TOKENS = ['on', 'to', 'set', 'be', 'an', "me"]


def parse_to_iso(dt, dt_hint=None):
    if dt_hint:
        parsed_dt = dateparser.parse(dt, date_formats=dt_hint)
    else:
        parsed_dt = dateparser.parse(dt)
    if parsed_dt:
        return parsed_dt.strftime(ISO_FORMAT)


def find_dates_in(text):
    rv = []
    dateparser_dates = search_dates(text)
    if dateparser_dates:
        rv += list(filter(_dateparser_search_filter, dateparser_dates))
    try:
        dateutil_dates = dutil_parse(text, fuzzy_with_tokens=True)
        ignored_tokens = dateutil_dates[1]
        text_analysed = ""
        for ignored_token in ignored_tokens:
            text_analysed += text.split(ignored_token)[0]
        rv.append([text_analysed, dateutil_dates[0]])
    except:
        ...
    return rv


def _dateparser_search_filter(dt_raw):
    return not (dt_raw[0].isdigit() or dt_raw[0] in SKIP_DATE_TOKENS)


class Location(object):
    """
    Represents a location with name, latitude and longitude
    """

    def __init__(self, name: str = None, latitude: float = 0.0, longitude: float = 0.0):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude

    def __lt__(self, other):
        return self.name < other.name

    def __str__(self):
        return "{} {} {}".format(self.name, self.latitude, self.longitude)


def normalize_words_to_number(query):
    """
    Converts queries like "in one hour" -> "in 1 hour"
    Assumes one-word numbers used
    """

    normal_list = []

    for word in query.split():
        try:
            normal_list.append(str(w2n.word_to_num(word)))
        except ValueError:
            normal_list.append(word)
    normal = ' '.join(normal_list)
    logger.debug("Normalized dt query: {} -> {}".format(query, normal))
    return normal


pytz_all_timezones = pytz.all_timezones
NORMALIZED_TZ_DICT = dict(zip([tz.lower() for tz in pytz_all_timezones], pytz_all_timezones))
TZ_OFFSETS = defaultdict(list)
TZ_OFFSETS_DST = defaultdict(list)
TZ_OFFSETS_NODST = defaultdict(list)

NORMALIZED_TZ_ABBR = defaultdict(set)
TZ_ABBRS_REVERSE = defaultdict(set)
for x_tz in pytz_all_timezones:
    dst_tzname = pytz.timezone(x_tz).localize(datetime.now(), is_dst=True).tzname()
    nodst_tzname = pytz.timezone(x_tz).localize(datetime.now(), is_dst=False).tzname()
    NORMALIZED_TZ_ABBR[dst_tzname.lower()].add(x_tz)
    NORMALIZED_TZ_ABBR[nodst_tzname.lower()].add(x_tz)
    TZ_ABBRS_REVERSE[x_tz].add(dst_tzname)
    TZ_ABBRS_REVERSE[x_tz].add(nodst_tzname)
    tz_offset = pytz.timezone(x_tz).localize(datetime.now()).strftime('%z')
    TZ_OFFSETS[tz_offset].append(x_tz)
    TZ_OFFSETS_DST[tz_offset].append(dst_tzname)
    TZ_OFFSETS_NODST[tz_offset].append(nodst_tzname)


def timezone_to_normal(query):
    """
    Makes a timezone written in wrong capitalization to correct one 
    as expected by IANA. E.g.:
        america/new_york -> America/New_York
    """
    # The magic in the regex is that it splits by either / OR _ OR -
    # where the | are OR; and then the parens ( ) keep the splitting
    # entries in the list so that we can join later

    normal = NORMALIZED_TZ_DICT.get(query, "")
    if not normal:
        normal = NORMALIZED_TZ_ABBR.get(query, "")
    logger.debug("Normalized timezone: {} -> {}".format(query, normal))
    return normal


def create_if_not_exists(fname):
    try:
        fh = open(fname, 'r')
    except FileNotFoundError:

        path = pathlib.Path(fname)
        path.parent.mkdir(parents=True, exist_ok=True)
        fh = open(fname, 'w')
    fh.close()


def write_to_cache(query, location):
    logger.debug("Writing location to cache")
    with open(os.path.join(basepath, "data", ".cache.csv"), 'a+') as wf:
        cachewriter = csv.writer(wf)
        cachewriter.writerow([query,
                              location.latitude,
                              location.longitude])


def row_to_location(row):
    """
    Row from a csv file to location class
    """
    latitude, longitude = float(row[1]), float(row[2])
    return Location(row[0], latitude, longitude)


def resolve_location_local(query):
    """
    Find a location by searching in local db of countries and cities
    """
    query = query.lower()
    create_if_not_exists(os.path.join(basepath, "data", ".cache.csv"))

    # location hypothesis heap
    heap = []

    for fname in [".cache", "countries", "cities"]:
        with open(os.path.join(basepath, "data", "{}.csv".format(fname))) as f:
            cfile = csv.reader(f)
            for row in cfile:
                entry = row[0]
                if fname == ".cache" and entry == query:
                    location = row_to_location(row)
                    logger.debug("Location (from cache): {}".format(location))
                    return location
                fuzz_ratio = fuzz.ratio(query, entry)
                if fuzz_ratio > FUZZ_THRESHOLD:
                    location = row_to_location(row)
                    logger.debug("Location hyp ({} {}): {}".format(fuzz_ratio, fname, location))
                    # need to push negative result as heapq is min heap
                    heappush(heap, (-fuzz_ratio, location))
    try:
        result = heappop(heap)
    except IndexError:
        logger.info("Could not find location {}".format(query))
        return ""
    ratio, location = result
    logger.debug("Location result ({}): {}".format(-ratio, location))
    write_to_cache(query, location)
    return location


def resolve_location_remote(query):
    user_agent = ''.join(random.choices(string.ascii_uppercase + string.digits, k=20))
    geolocator = Nominatim(user_agent=user_agent)
    try:
        location = geolocator.geocode(query)
        if location:
            write_to_cache(query, location)
            return location
    except GeocoderTimedOut:
        logger.info("Timed out resolving location. Try specifying a timezone directly")
    return Location()


def parse_query(query):
    """
    TODO: DEPRECATE THIS
    Parses the user query to the datetime, tz/loc parts
    """
    # query = ' '.join(query)
    query = query.strip()
    if not query:
        logger.info("TO DEPRECATE: Use a query like <datetime-like> ['to' <timezone or location>]")
    to_query = query.split(" to ")
    logger.debug("to_query: {}".format(to_query))
    if len(to_query) == 1:
        # only datetime
        human_dt, human_tz_loc = to_query[0], None
    elif len(to_query) == 2:
        # datetime to timezone
        human_dt, human_tz_loc = to_query
    else:
        logger.info("TO DEPRECATE: There can be only one 'to' in the query string")

    logger.debug("raw human_dt: {}".format(human_dt))
    logger.debug("raw human_tz_loc: {}".format(human_tz_loc))

    human_dt = normalize_words_to_number(human_dt)

    return human_dt, human_tz_loc


def serialize_location(location):
    return {
        "name": location.name if hasattr(location, 'name') else '',
        "latitude": location.latitude,
        "longitude": location.longitude,
    }


def find_from_offset(query):
    for universal_alias in ["gmt", "utc", "+", "-"]:
        if query.startswith(universal_alias):
            splitted_query = query.split(universal_alias)
            if len(splitted_query) != 2:
                continue
            offset = splitted_query[1]
            if ':' not in offset:
                if len(offset) == 4:
                    hhs, mms = offset[0:2], offset[2:4]
                else:
                    try:
                        hhs, mms = int(offset), "00"
                    except Exception:
                        continue
            else:
                splitted_offset = offset.split(':')
                if len(splitted_offset) != 2:
                    continue
                hhs, mms = splitted_offset
            try:
                if universal_alias in ["+", "-"]:
                    return tzinfo_from_offset("{}{:02d}{:02d}".format(universal_alias, int(hhs), int(mms)))
                return tzinfo_from_offset("+{:02d}{:02d}".format(int(hhs), int(mms)))
            except Exception:
                continue
    return None, []


def resolve_timezone(query):
    if not query:
        query = "utc"
    # if the human_tz_loc contains /, assume it's a timezone which could be
    # incorrectly written with small letters - need Continent/City
    normal_query = query.lower().strip()
    if normal_query in ['local']:
        local_iana = get_local_tzname_iana()
        local_offset = get_local_tz_offset()
        return {
            "query": query,
            "normal_query": normal_query,
            "tz_offset": local_offset,
            "tz_name": local_iana,
        }
    found_from_iana_tz = NORMALIZED_TZ_DICT.get(normal_query, "")
    found_from_abbr_tzs = list(NORMALIZED_TZ_ABBR.get(normal_query, set()))
    found_from_offset_tz, offset_tzs = find_from_offset(normal_query)
    normal_tz = found_from_iana_tz
    if not normal_tz:
        if found_from_abbr_tzs:
            normal_tz = list(found_from_abbr_tzs)[0]
        elif found_from_offset_tz:
            normal_tz = found_from_offset_tz.zone
    tz_abbrs = list(TZ_ABBRS_REVERSE.get(normal_tz, set()))
    logger.debug("Normalized timezone: {} -> {}".format(query, normal_tz))
    local_location, remote_location = {}, {}
    try:
        pytz_result = timezone(normal_tz)
    except UnknownTimeZoneError:
        logger.debug("No timezone: {}".format(normal_tz))
        # if the human_tz_loc contains /, assume it's a timezone
        # the timezone could still be guessed badly, attempt to get the city
        # e.g.america/dallas
        if "/" in query:
            logger.debug("Assuming wrongly guessed tz {}".format(query))
            query = query.split('/')[-1]
            logger.debug("Try city {}".format(query))
        # we don't know this timezone one, assume location
        # Try to get from local file first
        location = resolve_location_local(query)
        if location:
            local_location = serialize_location(location)
        else:
            # finally go to remote
            location = resolve_location_remote(query)
            if location:
                remote_location = serialize_location(location)
        tzf = TimezoneFinder()
        loc_tz = tzf.timezone_at(lat=location.latitude, lng=location.longitude)
        logger.debug("Timezone: {}".format(loc_tz))
        try:
            pytz_result = timezone(loc_tz)
        except UnknownTimeZoneError:
            pytz_result = type('pytz', (), {"zone": ""})
    tz_name = pytz_result.zone
    tz_abbr = pytz_result.localize(datetime.now()).strftime('%Z') if tz_name else ""
    tz_offset = pytz_result.localize(datetime.now()).strftime('%z') if tz_name else ""
    return {
        "query": query,
        "normal_query": normal_query,
        "found_from_iana_tz": found_from_iana_tz,
        "found_from_abbr_tzs": found_from_abbr_tzs,
        "found_from_offset_tzs": offset_tzs,
        "local_location": local_location,
        "remote_location": remote_location,
        "search_pytz": normal_tz,
        "tz_abbrs": tz_abbrs,
        "tz": normal_tz,
        "tz_name": tz_name,
        "tz_abbr": tz_abbr,
        "tz_offset": tz_offset,
    }


def solve_query(human_dt, human_tz_loc=None):
    try:
        # first try parsing the timezone from user input
        result = dateparser.parse(human_dt, settings={'RETURN_AS_TIMEZONE_AWARE': True, "TIMEZONE": "utc"})
        logger.debug("human_dt result: {}".format(result))
        if human_tz_loc:
            human_tz_loc = resolve_timezone(human_tz_loc)["tz_name"]
            isofmt = result.isoformat()
            logger.debug("human_dt isofmt: {}".format(isofmt))
            result = dateparser.parse(isofmt, settings={'TO_TIMEZONE': human_tz_loc})
            logger.debug("human_dt to_timezone result: {}".format(result))
    except UnknownTimeZoneError:
        loc_tz = resolve_timezone(human_tz_loc)["tz_name"]
        result = dateparser.parse(human_dt, settings={'TO_TIMEZONE': loc_tz})
    return result


def format_result(result, fmt):
    if result is None:
        logger.info("Could not solve query")
    logger.debug("Format: {}".format(fmt))
    format_result = result.strftime(fmt)
    logger.debug("Formated result: {} -> {}".format(result, format_result))
    return format_result


def query_to_format_result(query, fmt=DEFAULT_FORMAT):
    human_dt, human_tz_loc = parse_query(query)
    result = solve_query(human_dt, human_tz_loc)
    if fmt:
        return format_result(result, fmt)
    return result


def main(args):
    fmt = ISO_FORMAT if args.iso else args.format
    formated_result = query_to_format_result(args.query, fmt)
    print(formated_result)


def time_ago(date=None, diff=None, min_resolution=None, max_resolution=None):
    # TODO : create a min/max resolution
    now = get_utcnow()
    if not date:
        if diff:
            diff = timedelta(seconds=diff)
        else:
            diff = now - now
    else:
        if type(date) is str:
            parsed_dt = parse_dt(date)
            if parsed_dt.tzinfo is None:
                now = get_utcnow(tzaware=False)
            diff = now - parsed_dt
        elif type(date) is timedelta:
            diff = date
        elif type(date) is int:
            diff = now - datetime.fromtimestamp(date)
        elif isinstance(date, datetime):
            if date.tzinfo is not None:
                now = get_utcnow(tzaware=True)
            diff = now - date
        else:
            raise ValueError('invalid date %s of type %s' % (date, type(date)))

    sign = ''
    if diff.days < 0:
        diff = -diff
        sign = '-'
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff == 0:
        if second_diff < 60:
            return str("{}{:02d}".format(sign, second_diff))
        seconds = second_diff % 60
        if second_diff < 3600:
            min_diff = second_diff // 60
            return str("{}{:02d}:{:02d}".format(sign, min_diff, seconds))
        minutes = second_diff % 3600 // 60
        if second_diff < 86400:
            hrs_diff = second_diff // 3600
            return str("{}{:02d}:{:02d}:{:02d}".format(sign, hrs_diff, minutes,
                                                       seconds))
    seconds = second_diff % 60
    minutes = second_diff % 3600 // 60
    hours = second_diff // 3600
    if day_diff < 365:
        if day_diff < 30:
            return str("{}{}d{:02d}:{:02d}:{:02d}".format(sign, day_diff, hours,
                                                          minutes, seconds))
        months, days = day_diff // 30, day_diff % 30
        return str(
            "{}{}m{}d{:02d}:{:02d}:{:02d}".format(sign, months, days, hours,
                                                  minutes, seconds))
    years = day_diff // 365
    days = day_diff % 365
    months, days = days // 30, days % 30
    return str(
        "{}{}y{}m{}d{:02d}:{:02d}:{:02d}".format(sign, years, months, days,
                                                 hours, minutes, seconds))


def tzinfo_from_offset(offset: str) -> pytz.timezone:
    if ':' in offset:
        offset = ''.join(offset.split(':'))
    tznames = TZ_OFFSETS.get(offset, [])
    for tzname in tznames:
        if tzname.startswith('Etc/GMT'):
            return pytz.timezone(tzname), tznames
    if tznames:
        return pytz.timezone(tznames[0]), tznames
    return None, []


def custom_dt_parse(query):
    with open(os.path.join(basepath, "data", "custom_dt.csv")) as f:
        cfile = csv.reader(f)
        for row in cfile:
            entry = row[0]
            fuzz_query = query.lower().strip()
            fuzz_ratio = fuzz.ratio(fuzz_query, entry)
            if fuzz_ratio >= 95:
                return row[1]
    return query


r_next = re.compile('(?:next)\s*(.*)', flags=re.IGNORECASE)
r_prev = re.compile('(?:last|prev|previous)\s*(.*)', flags=re.IGNORECASE)
r_this = re.compile('(?:this|that)\s*(.*)', flags=re.IGNORECASE)


def get_local_now_parsed(s):
    now = get_local_now()
    now = now.replace(hour=0, minute=0, second=0, microsecond=0)
    parsed = parse_dt(s)
    parsed = parsed.replace(tzinfo=now.tzinfo)
    return now, parsed


def get_week_start_end(dt):
    start = dt - timedelta(days=dt.weekday())
    end = start + timedelta(days=6)
    return start, end


def handler_next_weekday(s):
    now, parsed = get_local_now_parsed(s)
    week_start, week_end = get_week_start_end(now)
    if parsed > week_end:
        # parsed is in next week
        return str(parsed)
    if week_start <= parsed <= week_end:
        # parsed is in this week
        return str(parsed + timedelta(days=7))
    else:
        # parsed is in previous week
        return str(parsed + timedelta(days=14))


def handler_prev_weekday(s):
    now, parsed = get_local_now_parsed(s)
    week_start, week_end = get_week_start_end(now)
    if parsed > week_end:
        # parsed is in next week
        return str(parsed - timedelta(days=14))
    if week_start <= parsed <= week_end:
        # parsed is in this week
        return str(parsed - timedelta(days=7))
    else:
        # parsed is in previous week
        return str(parsed)


def handler_this_weekday(s):
    now, parsed = get_local_now_parsed(s)
    week_start, week_end = get_week_start_end(now)
    if parsed > week_end:
        # parsed is in next week
        return str(parsed - timedelta(days=7))
    if week_start <= parsed <= week_end:
        # parsed is in this week
        return str(parsed)
    else:
        # parsed is in previous week
        return str(parsed + timedelta(days=7))


def try_regex(r, s):
    try:
        m = re.match(r, s)
    except:
        return None
    if m:
        groups = m.groups()
        return groups


regex_handlers = [
    (r_next, handler_next_weekday),
    (r_prev, handler_prev_weekday),
    (r_this, handler_this_weekday),
]


def regex_parsers(s):
    for r, h in regex_handlers:
        g = try_regex(r, s)
        if g is not None:
            try:
                result = h(*g)
            except Exception as e:
                continue
            if result is not None:
                return result
    return s


def dateparser_parse_dt(s: str):
    s = custom_dt_parse(s)
    s = regex_parsers(s)
    parsed = parse_dt(s)
    if not parsed:
        parsed = dutil_parse(s)
        if not parsed:
            return None
    if parsed.tzinfo is None:
        parsed = parsed.replace(tzinfo=tzinfo_from_offset(get_local_tz_offset())[0])
    return parsed


def get_utcnow(tzaware: bool = True):
    if tzaware:
        return datetime.utcnow().replace(tzinfo=pytz.UTC)
    return datetime.utcnow()


def get_local_now(tzaware: bool = True):
    if tzaware:
        return datetime.now().replace(tzinfo=tzinfo_from_offset(get_local_tz_offset())[0])
    return datetime.utcnow()


def split_offset(offset):
    if ':' in offset:
        to_shh, to_mm = offset.split(':')
    else:
        to_shh, to_mm = offset[:-2], offset[-2:]
    return int(to_shh), int(to_mm)


def dt_tz_translation(dt: datetime, to_tz_offset: str, from_tz_offset: str = "+00:00") -> datetime:
    to_shh, to_mm = split_offset(to_tz_offset)
    from_shh, from_mm = split_offset(from_tz_offset)
    tzinfo = tzinfo_from_offset(to_tz_offset)[0]
    if dt.tzinfo:
        return dt.astimezone(tzinfo)
    r_dt = dt + timedelta(hours=int(to_shh), minutes=int(to_mm)) - timedelta(
        hours=int(from_shh), minutes=int(from_mm))
    r_dt = tzinfo.localize(r_dt)
    return r_dt


def format_offset_from_timedelta(tdelta: timedelta):
    sign = "+" if tdelta.seconds >= 0 else "-"
    h = tdelta.seconds // 3600
    m = (tdelta.seconds // 60) - h * 60
    return "{}{:02d}:{:02d}".format(sign, h, m)


def get_local_tzname_iana():
    return '/'.join(os.path.realpath(gettz()._filename).split('/')[-2:])


def get_local_tz_offset():
    now = datetime.now(tzlocal())
    if now.tzinfo._isdst(now):
        return format_offset_from_timedelta(now.tzinfo._dst_offset)
    return format_offset_from_timedelta(now.tzinfo._std_offset)


def tzname_to_tz_offset(tzname_iana: str):
    return format_offset_from_timedelta(
        pytz.timezone(tzname_iana).utcoffset(get_utcnow(False)))


def get_dt_tz_offset(dt: datetime) -> timedelta:
    if dt.tzinfo is not None:
        if type(dt.tzinfo) is StaticTzInfo:
            tzoffset = dt.tzinfo._StaticTzInfo__offset
        else:
            try:
                tzoffset = dt.tzinfo._utcoffset
            except Exception as e:
                return dt.tzinfo.utcoffset(dt)
        return tzoffset
    return timedelta(0)


def get_us_since_epoch(dt: datetime):
    return int(dt.timestamp() * 1e6)


def get_ms_since_epoch(dt):
    return int(get_us_since_epoch(dt) / 1e3)


def get_s_since_epoch(dt):
    return int(get_us_since_epoch(dt) / 1e6)


def epoch_to_dt(seconds):
    return datetime.fromtimestamp(seconds)


def get_local_s_since_epoch(dt: datetime):
    utc_s = int(dt.timestamp())
    if dt.tzinfo is None:
        return utc_s
    local_s = int(get_dt_tz_offset(dt).seconds)
    total_s = utc_s + local_s
    return total_s


def time_to_emoji(dt):
    seconds = get_local_s_since_epoch(dt)
    a = int((seconds / 900 - 3) / 2 % 24)
    return chr(128336 + a // 2 + a % 2 * 12)


def workday_diff(start, end, workdays=None):
    """
    Calculates the difference between two dates excluding weekends
    """
    if start > end:
        start, end = end, start
    if not workdays:
        workdays = range(0, 5)
    td = end - start
    daygenerator = (start + timedelta(x + 1) for x in range(td.days))
    weekdays = sum(1 for day in daygenerator if day.weekday() in workdays)
    return timedelta(days=weekdays, seconds=td.seconds)


def workhours_diff(start, end, workhour_begin="09:00", workhour_end="17:00", workdays=None):
    """
    Calculates the difference between two dates excluding non-workhours
    This can potentially be very slow for long ranges as it calculates per minute resolution.
    """
    if start > end:
        start, end = end, start
    if not workdays:
        workdays = range(0, 5)

    workday_start_h, workday_start_m = map(int, workhour_begin.split(':'))
    workday_end_h, workday_end_m = map(int, workhour_end.split(':'))

    # assume night shift if next workday starts after
    day_diff = 1 if workday_end_h < workday_start_h else 0

    prev_dt_minute, dt_minute = start, start + timedelta(minutes=1)
    summins = 0
    while dt_minute < end:
        if dt_minute.weekday() not in workdays:
            prev_dt_minute, dt_minute = prev_dt_minute + timedelta(days=1), dt_minute + timedelta(days=1)
            continue
        this_day_workhours_begin = datetime(year=dt_minute.year, month=dt_minute.month, day=dt_minute.day,
                                            hour=workday_start_h, minute=workday_start_m, tzinfo=dt_minute.tzinfo)
        this_day_workhours_end = datetime(year=dt_minute.year, month=dt_minute.month, day=dt_minute.day,
                                          hour=workday_end_h, minute=workday_end_m, tzinfo=dt_minute.tzinfo)
        # calc if night shift
        this_day_workhours_end += timedelta(days=day_diff)

        # test if this minute is within workhours with daterange
        this_day_workhours = DateTimeRange(this_day_workhours_begin, this_day_workhours_end)
        time_range = DateTimeRange(prev_dt_minute, dt_minute)
        if time_range in this_day_workhours:
            # we are in workhours, add all the minutes here until the end (now) or end of workday - whichever is smaller
            end_delta = end if end < this_day_workhours_end else this_day_workhours_end
            summins += (end_delta - prev_dt_minute).total_seconds() // 60
            prev_dt_minute = end_delta
        else:
            # skip until next workday - naively add one day; it could be weekend, but it will be caught above
            prev_dt_minute = this_day_workhours_begin + timedelta(days=1)
        dt_minute = prev_dt_minute + timedelta(minutes=1)
    return timedelta(seconds=int(summins * 60))


def td_remainders(td):
    # split seconds to larger units
    seconds = td.total_seconds()
    minutes, seconds = divmod(abs(int(seconds)), 60)
    hours, minutes = divmod(abs(int(minutes)), 60)
    days, hours = divmod(abs(int(hours)), 24)
    months, days = divmod(abs(int(days)), 30.42)
    years, months = divmod(abs(int(months)), 12)
    years, months, days, hours, minutes, seconds = map(int, (years, months, days, hours, minutes, seconds))
    years, months, days, hours, minutes, seconds = map(abs, (years, months, days, hours, minutes, seconds))
    return dict(
        seconds=seconds,
        minutes=minutes,
        hours=hours,
        days=days,
        months=months,
        years=years,
    )


def td_totals(td):
    seconds = td.total_seconds()
    minutes = seconds / 60
    hours = seconds / (60 * 60)
    days = seconds / (24 * 60 * 60)
    weeks = seconds / (7 * 24 * 60 * 60)
    months = seconds / (30 * 24 * 60 * 60)
    years = seconds / (365 * 24 * 60 * 60)
    years, months, weeks, days, hours, minutes, seconds = map(abs,
                                                              (years, months, weeks, days, hours, minutes, seconds))
    return dict(
        seconds=seconds,
        minutes=minutes,
        hours=hours,
        days=days,
        weeks=weeks,
        months=months,
        years=years,
    )


def td_iso8601(td):
    """P[n]Y[n]M[n]DT[n]H[n]M[n]S"""
    rem = td_remainders(td)
    fmt = "P"
    for short, timeframe in [("Y", "years"), ("M", "months"), ("D", "days")]:
        if rem[timeframe]:
            fmt += "{}{}".format(rem[timeframe], short)
    hms = [("H", "hours"), ("M", "minutes"), ("S", "seconds")]
    if any([rem[t[1]] for t in hms]):
        fmt += "T"
        for short, timeframe in hms:
            if rem[timeframe]:
                fmt += "{}{}".format(rem[timeframe], short)
    return fmt


if __name__ == "__main__":
    args = parse_args()
    setup_logging_level(args.debug)
    main(args)
