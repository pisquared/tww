import datetime
import unittest

from freezegun import freeze_time

from tww import query_to_format_result, setup_logging_level


def get_local_hours_offset():
    now = datetime.datetime.now()
    local_tzinfo = now.astimezone().tzinfo
    delta = local_tzinfo.utcoffset(now)
    return delta.total_seconds() / 3600


FROZEN_TIME = "2015-03-14 09:26:53 UTC"

# TODO: not quite sure....
# This gives the UTC+XX.YY offset.
# In order to get frozen time in UTC, use the negative as offset
UTC = -get_local_hours_offset()

# LocalTimezone, Query, Result
INTEGRATION_TESTS = [
    (UTC, "now", "2015-03-14 09:26:53"),
    # in two hours -> 11:26 | to tokyo +9 -> 20:26
    (0, "in two hours to asia/tokyo", "2015-03-14 20:26:53"),
    # now in UTC +2hrs -> 11:26
    (0, "now to sofia", "2015-03-14 11:26:53"),
]


class IntegrationTests(unittest.TestCase):
    def setUp(self):
        setup_logging_level(True)

    def test_integration(self):
        for test in INTEGRATION_TESTS:
            local_tz = test[0]
            query = test[1].split(" ")
            result = test[2]

            freezer = freeze_time(FROZEN_TIME, tz_offset=local_tz)

            freezer.start()
            self.assertEqual(query_to_format_result(query), result)
            freezer.stop()


if __name__ == '__main__':
    unittest.main()
