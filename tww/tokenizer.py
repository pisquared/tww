import argparse
import json
import locale
import re
import logging
from datetime import datetime

from pygments import highlight, lexers, formatters
from scalpl import Cut

from tww.localization import setlocale, resolve_locale
from tww.lib import ISO_FORMAT, time_to_emoji, time_ago, workday_diff, workhours_diff, td_remainders, td_totals, \
    td_iso8601, ISO_Z_FORMAT
from tww.lib import resolve_timezone, dateparser_parse_dt, get_utcnow, get_s_since_epoch, get_ms_since_epoch, \
    dt_tz_translation, get_local_now, query_to_format_result
from tww.common import logger

custom_locale = resolve_locale()

r_generic = re.compile('(.*)', flags=re.IGNORECASE)
r_time_in_epoch_s_now = re.compile('(?:time since epoch|seconds since epoch)', flags=re.IGNORECASE)
r_time_in_epoch_s2 = re.compile('(.*)?\s*(?:in|to)\s*(?:epoch|seconds since epoch|seconds)', flags=re.IGNORECASE)
r_time_in_epoch_s3 = re.compile('(?:seconds)?\s*since\s*(.*)', flags=re.IGNORECASE)
r_time_in_epoch_ms_now = re.compile('(?:milliseconds since epoch)', flags=re.IGNORECASE)
r_time_in_epoch_ms2 = re.compile('(.*)?\s*(?:in|to)\s*(?:ms|milliseconds|miliseconds)', flags=re.IGNORECASE)
r_time_in_epoch_ms3 = re.compile('(?:ms|milliseconds|miliseconds)?\s*since\s*(.*)', flags=re.IGNORECASE)
r_time_in = re.compile('(?:time)?\s*in\s*(.*)', flags=re.IGNORECASE)
r_time_since = re.compile('(?:time)?\s*since\s*(.*)', flags=re.IGNORECASE)
r_time_until = re.compile('(?:time)?\s*until\s*(.*)', flags=re.IGNORECASE)
r_time_between = re.compile('(?:time)?\s*between\s*(.*)\s*and\s*(.*)', flags=re.IGNORECASE)
r_workdays_since = re.compile('(?:workdays|work days)\s*since\s*(.*)', flags=re.IGNORECASE)
r_workdays_until = re.compile('(?:workdays|work days)\s*until\s*(.*)', flags=re.IGNORECASE)
r_workdays_between = re.compile('(?:workdays|work days)\s*between\s*(.*)\s*and\s*(.*)', flags=re.IGNORECASE)
r_workhours_since = re.compile('(?:workhours|work hours)\s*since\s*(.*)', flags=re.IGNORECASE)
r_workhours_until = re.compile('(?:workhours|work hours)\s*until\s*(.*)', flags=re.IGNORECASE)
r_workhours_between = re.compile('(?:workhours|work hours)\s*between\s*(.*)\s*and\s*(.*)', flags=re.IGNORECASE)
r_timezone_translation = re.compile('(.*)?\s(?:in|to)\s(.*)', flags=re.IGNORECASE)
r_timezone = re.compile('(.*)\s(?:timezone|timezones|tz)', flags=re.IGNORECASE)
r_timezone_2 = re.compile('(?:timezone in|timezones in|tz in|timezone|timezones|tz)\s(.*)?', flags=re.IGNORECASE)


def handler_time(dt_s):
    return dateparser_parse_dt(dt_s)


def handler_time_now_local():
    return get_local_now()


def handler_time_now_utc():
    return get_utcnow()


def dt_normalize(start_dt, end_dt) -> (datetime, datetime):
    if type(start_dt) is str:
        start_dt = dateparser_parse_dt(start_dt)
    if type(end_dt) is str:
        end_dt = dateparser_parse_dt(end_dt)
    return start_dt, end_dt


def handler_time_diff(start_dt, end_dt) -> dict:
    start_dt, end_dt = dt_normalize(start_dt, end_dt)
    diff = start_dt - end_dt
    return dict(start=dt_pretty(start_dt),
                end=dt_pretty(end_dt),
                diff=td_pretty(diff))


def handler_time_since_until(start_dt_s: str) -> dict:
    return handler_time_diff(start_dt_s, get_local_now())


def handler_workdays_diff(start_dt, end_dt) -> dict:
    start_dt, end_dt = dt_normalize(start_dt, end_dt)
    diff = workday_diff(start_dt, end_dt)
    return dict(start=dt_pretty(start_dt),
                end=dt_pretty(end_dt),
                diff=td_pretty(diff))


def handler_workdays_since_until(start_dt_s: str) -> dict:
    return handler_workdays_diff(dateparser_parse_dt(start_dt_s), get_local_now())


def handler_workhours_diff(start_dt, end_dt) -> dict:
    start_dt, end_dt = dt_normalize(start_dt, end_dt)
    diff = workhours_diff(start_dt, end_dt)
    return dict(start=dt_pretty(start_dt),
                end=dt_pretty(end_dt),
                diff=td_pretty(diff))


def handler_workhours_since_until(start_dt_s: str) -> dict:
    return handler_workhours_diff(dateparser_parse_dt(start_dt_s), get_local_now())


def handler_timezone_translation(dt_s: str, timezone_like_s: str) -> dict:
    if dt_s.lower().strip() == "time":
        dt_s = "now"
    src_dt = dateparser_parse_dt(dt_s)
    tz = resolve_timezone(timezone_like_s)
    if not tz:
        tz, dst_dt = {}, src_dt
    else:
        offset = tz.get('tz_offset')
        dst_dt = dt_tz_translation(src_dt, offset)
    return src_dt, dst_dt, tz


def handler_generic_parser(dt_s: str) -> datetime:
    return dateparser_parse_dt(dt_s)


def handler_time_in_parser(dt_s: str) -> datetime:
    return query_to_format_result("now to {}".format(dt_s), None)


def handler_timezone(timezone_s: str):
    return resolve_timezone(timezone_s)


QUERY_TYPE_DT_TR = "datetime_translation"
QUERY_TYPE_DT = "datetime_details"
QUERY_TYPE_TZ = "timezone"
QUERY_TYPE_TD = "timedelta"

h_default = ''
h_unix_s = 'dt->unix_s'
h_unix_ms = 'dt->unix_ms'
h_tz_offset = 'tz->tz_offset'
h_time_in = 'dt->hh:mm'
h_translation = 'dt->iso8601_full'
h_default_dt = 'dt->iso8601_full'
h_default_td = 'timedelta->diff->duration_iso8601'

regex_handlers = [
    (r_time_in_epoch_s_now, handler_time_now_local, QUERY_TYPE_DT, h_unix_s),
    (r_time_in_epoch_s_now, handler_time_now_utc, QUERY_TYPE_DT, h_unix_s),
    (r_time_in_epoch_s2, handler_time, QUERY_TYPE_DT, h_unix_s),
    (r_time_in_epoch_s3, handler_time, QUERY_TYPE_DT, h_unix_s),
    (r_time_in_epoch_ms_now, handler_time_now_local, QUERY_TYPE_DT, h_unix_ms),
    (r_time_in_epoch_ms_now, handler_time_now_utc, QUERY_TYPE_DT, h_unix_ms),
    (r_time_in_epoch_ms2, handler_time, QUERY_TYPE_DT, h_unix_ms),
    (r_time_in_epoch_ms3, handler_time, QUERY_TYPE_DT, h_unix_ms),
    (r_timezone_translation, handler_timezone_translation, QUERY_TYPE_DT_TR, h_translation),
    (r_time_since, handler_time_since_until, QUERY_TYPE_TD, h_default_td),
    (r_time_until, handler_time_since_until, QUERY_TYPE_TD, h_default_td),
    (r_time_between, handler_time_diff, QUERY_TYPE_TD, h_default_td),
    (r_workdays_since, handler_workdays_since_until, QUERY_TYPE_TD, h_default_td),
    (r_workdays_until, handler_workdays_since_until, QUERY_TYPE_TD, h_default_td),
    (r_workdays_between, handler_workdays_diff, QUERY_TYPE_TD, h_default_td),
    (r_workhours_since, handler_workhours_since_until, QUERY_TYPE_TD, h_default_td),
    (r_workhours_until, handler_workhours_since_until, QUERY_TYPE_TD, h_default_td),
    (r_workhours_between, handler_workhours_diff, QUERY_TYPE_TD, h_default_td),
    (r_time_in, handler_time_in_parser, QUERY_TYPE_DT, h_time_in),
    (r_timezone, handler_timezone, QUERY_TYPE_TZ, h_tz_offset),
    (r_timezone_2, handler_timezone, QUERY_TYPE_TZ, h_tz_offset),
    (r_generic, handler_generic_parser, QUERY_TYPE_DT, h_default_dt),
]


def try_regex(r, s):
    try:
        m = re.match(r, s)
    except:
        return None
    if m:
        groups = m.groups()
        return groups


def tokenize(s):
    solutions = []
    for r, h, t, hi in regex_handlers:
        g = try_regex(r, s)
        if g is not None:
            try:
                result = h(*g)
            except Exception as e:
                continue
            if result is not None:
                solutions.append((h.__name__, result, t, hi))
    return solutions


def pretty_print_dict(obj):
    formatted_json = json.dumps(obj, indent=2, ensure_ascii=False)
    colorful_json = highlight(formatted_json, lexers.JsonLexer(), formatters.TerminalFormatter())
    print(colorful_json)


def show_magic_results(obj, args):
    for solution in obj['solutions']:
        entry_proxy = Cut(solution, sep='->')
        highlight_entry = solution["highlight"]
        try:
            highlight_result = entry_proxy[highlight_entry]
        except Exception as e:
            continue
        if args.handlers:
            print("{} -> {}".format(solution['handler'], highlight_result))
        else:
            print(highlight_result)


def dt_pretty(dt):
    rv = {}
    global custom_locale
    rv["iso8601_full"] = dt.strftime(ISO_FORMAT)
    rv["iso8601_utc"] = dt_tz_translation(dt, to_tz_offset='+00:00',
                                          from_tz_offset=dt.strftime('%z')).strftime(ISO_Z_FORMAT)
    rv["iso8601_date"] = dt.strftime('%Y-%m-%d')
    rv["iso8601_time"] = dt.strftime('%H:%M:%S')
    rv["locale_dt"] = dt.strftime("%c")
    rv["day_of_week_number"] = dt.strftime("%w")
    rv["locale"] = custom_locale
    with setlocale(locale.LC_TIME, custom_locale.get("lc_time")):
        rv["locale_month"] = dt.strftime("%B")
        rv["locale_month_short"] = dt.strftime("%b")
        rv["locale_day_of_week_short"] = dt.strftime("%a")
        rv["locale_day_of_week"] = dt.strftime("%A")
        rv["locale_date"] = dt.strftime("%x")
        rv["locale_time"] = dt.strftime("%X")
    rv["tz_offset"] = dt.strftime("%z")
    rv["hh:mm"] = dt.strftime("%H:%M")
    rv["emoji_time"] = time_to_emoji(dt)
    rv["unix_s"] = get_s_since_epoch(dt)
    rv["unix_ms"] = get_ms_since_epoch(dt)
    return rv


def td_pretty(td):
    rv = {
        "sign": '-' if td.days > 0 else '+',
        "in_the": 'future' if td.days < 0 else 'past',
        "time_ago": time_ago(td),
        "duration_iso8601": td_iso8601(td),
        "totals": td_totals(td),
        "remainders": td_remainders(td),
    }
    return rv


def resolve_query_type(query):
    solutions = tokenize(query)
    if not solutions:
        dt = get_local_now()
        return [["now", dt, QUERY_TYPE_DT, h_default_dt]]
    return solutions


def resolve_query(query, allowed_queries=None):
    rv = {
        "query": query,
        "solutions": [],
    }
    solutions = resolve_query_type(query)
    if not allowed_queries:
        allowed_queries = [QUERY_TYPE_DT, QUERY_TYPE_DT_TR, QUERY_TYPE_TD, QUERY_TYPE_TZ]
    for sol_id, solution in enumerate(solutions):
        element = {}
        handler, results, query_type, hi = solution
        element["handler"] = handler
        element["query_type"] = query_type
        element["highlight"] = hi
        element["solution_id"] = sol_id
        try:
            if query_type not in allowed_queries:
                continue
            if query_type == QUERY_TYPE_DT:
                element["dt"] = dt_pretty(results)
            elif query_type == QUERY_TYPE_DT_TR:
                element["src_dt"] = dt_pretty(results[0])
                element["dt"] = dt_pretty(results[1])
                element["tz"] = results[2]
            elif query_type == QUERY_TYPE_TZ:
                element["tz"] = results
            elif query_type == QUERY_TYPE_TD:
                element["timedelta"] = results
            rv["solutions"].append(element)
        except Exception as e:
            continue
    return rv


def test():
    test_strings = [
        None,
        "",
        "s",
        "    ",
        "Time  since 2019-05-12",
        "Since yesterday",
        "time between yesterday and tomorrow",
        "time until 25 december",
        "time sinc",
        "now in milliseconds",
        "seconds since epoch",
        "1992-01-27 to epoch",
        "milliseconds since 1992-01-27",
        "now in sofia",
        "now in PST",
        "2 hours ago to Sydney",
        "now in +03:00",
        "now in dublin",
        "workdays since 2/07/2020 12:00",
        "workhours since 2/07/2020 12:00",
    ]
    for s in test_strings:
        print("{} -> {}".format(s, resolve_query(s)))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('query', nargs='*', default="", help="freeform")
    parser.add_argument('--locale', dest='locale')
    parser.add_argument('--handlers', dest='handlers', action='store_true')
    parser.add_argument('--full', dest='full', action='store_true')
    parser.add_argument('--debug', dest='debug', action='store_true')
    args = parser.parse_args()
    return args


def setup_logging_level(debug=False):
    log_level = logging.DEBUG if debug else logging.ERROR
    logger.setLevel(log_level)
    logger.debug("Debugging enabled")


def main(args):
    global custom_locale
    custom_locale = resolve_locale(args.locale)
    query = ' '.join(args.query)
    # query = "tz in sofia"
    result = resolve_query(query)
    if args.full:
        pretty_print_dict(result)
    show_magic_results(result, args)
