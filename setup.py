from setuptools import setup, find_packages


def gen_from_file():
    with open("requirements.txt") as f:
        return [line.strip() for line in f.readlines()]


setup(name='tww',
      version='0.2',
      description='Time When and Where',
      url='http://gitlab.com/pisquared/tww',
      author='Daniel Tsvetkov',
      install_requires=gen_from_file(),
      author_email='',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)
