# Time when and where
Find time now, in the past or future in any timezone or location.

## Usage
```
python tz.py QUERY [--format="%Y-%m-%d %H:%M:%S%z"] [--debug]
```

* `QUERY` - is of the form `<datetime-like> to <timezone or location>`

`<datetime-like>` is any time or time-like string - or example `2019-04-26 3:14`, `06:42`, timezones `15:10 cet` but also some human readable like `now`, `in 3 hours`, `7 minutes ago` and many others. See [dateparser](https://pypi.org/project/dateparser/) for more.

`to <timezone or location>` is optional. It has to have the word `to` which specifies that timezone or location follows. It is either:
  - timezone (tried first) to which the date should be translated or 
  - a location. Uses a local database of files of countries and cities. It then tries to fuzzymatch the query using [fuzzywuzzy](https://github.com/seatgeek/fuzzywuzzy). In case it can't find the country or city, it uses [geopy](https://geopy.readthedocs.io/en/stable/) for location resolution. Finally it uses [timezonefinder](https://pypi.org/project/timezonefinder/) for timezone resolution.

* `--format` is the format of the time to be displayed. See supported [datetime formats](https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior)

## Install

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

You could alias the whole command to `tww` for faster typing, e.g. in your `.bashrc`:

```
alias tww="~/workspace/tz/venv/bin/python ~/workspace/tz/tz.py"
```

## Examples

Time now (in this timezone):

```
$ tww now
2019-03-13 15:04:36
```

Time now to another timezone (UTC let's say):
```
$ tww now to utc
2019-03-13 15:04:36
```

One hour from now in UTC, showing only the time:
```
$ tww in 1 hour to cet --format="%T"
23:17:49
```

With timezone:
```
$ tww now to asia/tokyo
2019-03-14 07:06:35
```

Another time to timezone:
```
$ tww 15:10 to cet
2019-03-13 23:10:00
```

Time in one timezone (pst) to another in city:
```
$ tww 3/14 15 9:26:53 PST to sofia
2015-03-14 19:26:53+02:00
```
